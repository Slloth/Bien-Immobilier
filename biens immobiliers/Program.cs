﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace biens_immobiliers
{
	class Program
	{
		static List<Realty> RealtyList;

		static void Main(string[] args)
		{
			RealtyList = new List<Realty>();
			Menu();
		}
		public static void Menu()
		{
			int totalRealty = RealtyList.Count;
			Console.WriteLine("Gestion du parc immoblier de l'agence\nLe stock immoblier d l'agence est, actuellement, de : " + totalRealty);
			Console.WriteLine("\nMenu de l'application\n");
			Console.WriteLine("1 : Afficher la listes des biens");
			Console.WriteLine("2 : Ajouter un appartement");
			Console.WriteLine("3 : Ajouter une maison");
			Console.WriteLine("4 : Ajouter un parking");
			Console.WriteLine("5 : Supprimer un bien");
			Console.WriteLine("6 : Quitter l'application");
			int Index = int.Parse(Console.ReadLine());
			switch (Index)
			{	//Récupère l'input est apelle les Methodes correspondante
				case 1:
					Console.Clear();
					realtyList();
					break;
				case 2:
					Console.Clear();
					addFlat();
					break;
				case 3:
					Console.Clear();
					addHome();
					break;
				case 4:
					Console.Clear();
					addParking();
					break;
				case 5:
					Console.Clear();
					removeRealty();
					break;
				case 6:
					Console.Clear();
					Environment.Exit(0);
					break;
				default:
					Console.WriteLine("Erreur");
					break;
			};
		}
		public static void addFlat()
		{
			Console.WriteLine("Ajout d'un appartement : ");
			Console.WriteLine("Numéro d'enregistrement : ");
			int registerNumber = int.Parse(Console.ReadLine());
			Console.WriteLine("Localisation du bien : ");
			string location = Console.ReadLine();
			Console.WriteLine("Superficie (en m²) : ");
			int area = int.Parse(Console.ReadLine());
			Console.WriteLine("Loyer ( en €) : ");
			double rent = double.Parse(Console.ReadLine());
			Console.WriteLine("Nombre de piéce : ");
			int rooms = int.Parse(Console.ReadLine());
			Console.WriteLine("Etage : ");
			int floors = int.Parse(Console.ReadLine());
			//Incrémente les valeurs entré par l'utilisateur dans les variables de la classe Flat PS : les valeurs entrer doivent étres dans l'ordre
			Flat flat = new Flat(registerNumber, area, location, rent, rooms, floors);
			RealtyList.Add(flat);
			Console.Clear();
			Menu();
		}
		public static void addHome()
		{
			Console.WriteLine("Ajout d'une maison : ");
			Console.WriteLine("Numéro d'enregistrement : ");
			int registerNumber = int.Parse(Console.ReadLine());
			Console.WriteLine("Localisation du bien : ");
			string location = Console.ReadLine();
			Console.WriteLine("Superficie (en m²) : ");
			int area = int.Parse(Console.ReadLine());
			Console.WriteLine("Loyer ( en €) : ");
			double rent = double.Parse(Console.ReadLine());
			Console.WriteLine("Nombre de piéce : ");
			int rooms = int.Parse(Console.ReadLine());
			//Incrémente les valeurs entré par l'utilisateur dans les variables de la classe Home PS : les valeurs entrer doivent étres dans l'ordre
			Home home = new Home(registerNumber, area, location, rent, rooms);
			RealtyList.Add(home);
			Console.Clear();
			Menu();
		}
		public static void addParking()
		{
			Console.WriteLine("Ajout d'un Parking : ");
			Console.WriteLine("Numéro d'enregistrement : ");
			int registerNumber = int.Parse(Console.ReadLine());
			Console.WriteLine("Localisation du bien : ");
			string location = Console.ReadLine();
			Console.WriteLine("Superficie (en m²) : ");
			int area = int.Parse(Console.ReadLine());
			Console.WriteLine("Loyer ( en €) : ");
			double rent = double.Parse(Console.ReadLine());
			//Incrémente les valeurs entré par l'utilisateur dans les variables de la classe Parking PS : les valeurs entrer doivent étres dans l'ordre
			Parking parking = new Parking(registerNumber, area, location, rent);
			RealtyList.Add(parking);
			Console.Clear();
			Menu();
		}
		public static void realtyList()
		{	//affichage de la liste
			for (int indice = 0; indice < RealtyList.Count; indice++)
			{
				Console.WriteLine("\n"+ RealtyList[indice].ToString());

			}
			Menu();
		}
		public static void removeRealty()
		{
			Console.WriteLine("Supprimer un Bien ? ");
			string toDelete = Console.ReadLine();
			Realty realtyToRemove = null;
			//Permet de convertir le ToDelete en INT si il est numérique
			if (int.TryParse(toDelete, out int value))
			{	//Suppresion de l'élement désier
				foreach (var texte in RealtyList)
				{
					if(texte.RegisterNumber == value)
					{
						realtyToRemove = texte;
					}
					//si il est nul
					if(realtyToRemove == null)
					{
						Console.WriteLine("Le bien demandé n'existe pas ou plus, ou peut etre jamais.");
					}
					else
					{
						RealtyList.Remove(realtyToRemove);
						Console.WriteLine("Le bien a été supprimé");
						Console.Clear();
						Menu();
					}
				}
			}
		}
	}
}