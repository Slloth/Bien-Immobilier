﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace biens_immobiliers
{
	public abstract class Realty
	{
		protected int registerNumber, area;
		protected string location;
		protected double rent;
		public int RegisterNumber {get => registerNumber;}
		public Realty(int registerNumber, int area, string location, double rent)
		{
			this.registerNumber = registerNumber;
			this.area = area;
			this.location = location;
			this.rent = rent;
		}
		public override string ToString()
		{
			string display = "Nombre du bien" + registerNumber + "\n Localisation du bien : " + location + "\n Superficie (en m²) : " + area + "\n Loyer ( en €) : " + rent;
			return display;
		}
	}
}