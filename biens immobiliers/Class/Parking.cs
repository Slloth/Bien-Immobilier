﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace biens_immobiliers
{
	public class Parking : Realty
	{
		public Parking(int registerNumber, int area, string location, double rent) : base(registerNumber, area, location, rent)
		{
			this.registerNumber = registerNumber;
			this.area = area;
			this.location = location;
			this.rent = rent;
		}
	}
}
