﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace biens_immobiliers
{
	public class Flat : Realty
	{
		private int rooms, floor;
		public Flat(int registerNumber, int area, string location, double rent, int rooms, int floor) : base(registerNumber, area, location, rent)
		{
			this.registerNumber = registerNumber;
			this.area = area;
			this.location = location;
			this.rent = rent;
			this.rooms = rooms;
			this.floor = floor;
		}
		public override string ToString()
		{
			string display = "Nombre du bien" + registerNumber + "\n Localisation du bien : " + location + "\n Superficie (en m²) : " + area + "\n Loyer ( en €) : " + rent + "\n Nombre de piéce : " + rooms + "\n Etage : " + floor;
			return display;
		}
	}
}